require('dotenv/config')
const port = process.env.PORT ;
const express = require('express');
const app = express();
// logs imports requirements
const morgan = require('morgan');
const fs = require('fs');
const path = require('path');
// cors origins policy requirements
const cors = require('cors');
//import the log file 
const logEvents = require('./middleware/logEvents');
// create a write stream (in append mode) to log requests to a file
// const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs', 'access.log'), { flags: 'a' });
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs', 'access.log'), {flags: 'a'})
// set up the morgan middleware to log HTTP requests to the access.log file
app.use(morgan('combined', { stream: accessLogStream }));
// require the logEvents middleware function from the middleware directory

// use the logEvents middleware function to log custom events to the access.log file
app.use(logEvents);

// set up a basic route for testing
app.get('/', (req, res) => {
  res.send('Hello, world!');
});

//custom middleware logger
app.use((req, res, next)=>{
    console.log(`${req.method} ${req.path}`)
    next();
})

//serve static files
app.use(express.static('./public', {root: __dirname}))





app.get('/index', (req, res)=>{
    res.sendFile('./views/index.html', {root : __dirname})
});
app.get('/new-page', (req, res)=>{
    res.sendFile('./views/new-page.html', {root : __dirname})
});

app.get('/old-page', (req,res)=>{
    res.sendFile( 'views/new-page.html', {root : __dirname});
});

// route handler
app.get('hello', (req, res, next)=>{
    console.log('attempting to lead to hello.html')
    next()
},(req, res)=>{
    res.send('hello world');
})

const one =(req, res, next)=>{
    console.log('one');
    next();
}
const two =(req, res, next)=>{
    console.log('two');
    next();
}

const three =(req, res)=>{
    console.log('three');
    res.send('finished');
}
// chain function method
app.get('/chain', [one, two, three]);
// app.get('/*', (req, res)=>{
//     res.status(404).sendFile( 'views/404.html', {root : __dirname})
// })


const notAllowedSites = ['example1', 'example2']
const corsOptions ={
    origin : (origin , callback) => {
        if(! notAllowedSites.includes(origin)){
            callback(null, true)
        }
        else{
            callback( new Error(`Origin ${origin} not allowed by ayman's policy`))
        }
    }
}
app.use(cors(corsOptions))

app.listen(port, ()=>{ console.log(`listening on port ${port} `)});