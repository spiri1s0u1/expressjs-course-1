const fs = require('fs');
const path = require('path');

function logEvents(req, res, next) {
  // create a write stream (in append mode) to log custom events to the same file as the access logs
  const accessLogStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', 'access.log'), { flags: 'a' });

  // log a custom event to the access log file
  accessLogStream.write(`Custom event logged: ${Date.now()}\n`);

  next();
}

module.exports = logEvents;
